//
//  PricingInfoAdapter.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

protocol PricingInfoAdapterProtocol {
    func adapt(_ response: PricingInfoResponse) -> PricingInfo
}

struct PricingInfoAdapter: PricingInfoAdapterProtocol {
    
    func adapt(_ response: PricingInfoResponse) -> PricingInfo {
        PricingInfo(price: response.price?.doubleValue() ?? 0,
                    iptu: response.iptu?.doubleValue() ?? 0,
                    businessType: getBusinessType(response),
                    condoFee: response.condoFee?.optionalDoubleValue())
    }
    
    func getBusinessType(_ response: PricingInfoResponse) -> BusinessType {
        switch response.businessType {
        case "SALE", "sale":
            return .sale
        case "RENTAL", "rental":
            return .rental
        default:
            return .unknowed
        }
    }
}
