//
//  PropertyAdapter.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

protocol PropertyAdapterProtocol {
    func adapt(_ response: PropertyResponse) -> Property
}


struct PropertyAdapter {
    
    let addressAdapter: AddressAdapterProtocol
    let pricingInfoAdapter: PricingInfoAdapterProtocol
    
    init(pricingInfoAdapter: PricingInfoAdapterProtocol = PricingInfoAdapter(),
         addressAdapter: AddressAdapterProtocol = AddressAdapter()) {
        self.pricingInfoAdapter = pricingInfoAdapter
        self.addressAdapter = addressAdapter
    }
    
    func adapt(_ response: PropertyResponse) -> Property {
        let address: Address = addressAdapter.adapt(response.address)
        let pricingInfo: PricingInfo = pricingInfoAdapter.adapt(response.pricingInfos)
        var property = Property(area: response.area,
                        id: response.id,
                        parkingSpaces: response.parkingSpaces ?? 0,
                        images: response.images,
                        bedrooms: response.bathrooms ?? 0,
                        bathrooms: response.bathrooms ?? 0,
                        address: address,
                        pricingInfo: pricingInfo,
                        portals: Set(Portal.allCases))
        
        property.assignPortals()
        property.filterPortals()
        property.checkPriceChanges()
        return property
    }
}
    
    
fileprivate extension Property {
    
    mutating func assignPortals() {
        switch pricingInfo.businessType {
        case .rental:
            if pricingInfo.price < 3500 {
               portals.remove(.zap)
            }
            if pricingInfo.price > 4000 {
                portals.remove(.vivareal)
            }
        case .sale:
            if pricingInfo.price < 600000 {
                portals.remove(.zap)
            }
            if pricingInfo.price > 700000 {
                portals.remove(.vivareal)
            }
        case .unknowed:
            portals.removeAll()
        }
    }
    
    
    mutating func filterPortals() {
        if address.latitude == 0 && address.longitude == 0 {
            portals.removeAll()
            return
        }
        if pricingInfo.businessType == .sale && portals.contains(.zap) {
            if area == 0 || squareMeterValue <= 3500 {
                portals.remove(.zap)
                return
            }
        }
        else if pricingInfo.businessType == .rental && portals.contains(.vivareal) {
            guard let condoFeeProportion = condoFeeProportion else {
                portals.remove(.vivareal)
                return
            }
            if condoFeeProportion >= 30 || pricingInfo.condoFee == nil {
                portals.remove(.vivareal)
                return
            }
        }
    }
    
    mutating func checkPriceChanges() {
        let starterPrice = pricingInfo.price
        if portals.contains(.zap) && pricingInfo.businessType == .sale && isInZapBoundbox {
            pricingInfo.price = starterPrice * 0.9
        }
        if portals.contains(.vivareal) && pricingInfo.businessType == .rental && isInZapBoundbox {
            return pricingInfo.price = starterPrice + (starterPrice * 0.5)
        }
    }
}

