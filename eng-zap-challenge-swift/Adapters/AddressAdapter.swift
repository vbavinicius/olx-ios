//
//  AddressAdapter.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

protocol AddressAdapterProtocol {
    func adapt(_ response: AddressResponse) -> Address
}

struct AddressAdapter: AddressAdapterProtocol {
    func adapt(_ response: AddressResponse) -> Address {
        Address(city: response.city,
                neighborhood: response.neighborhood,
                longitude: response.geoLocation.location.longitude,
                latitude: response.geoLocation.location.latitude)
    }
}
