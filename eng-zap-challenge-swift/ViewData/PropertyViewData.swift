//
//  PropertyViewData.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

struct PropertyViewData {
    var businessType: String = ""
    var price: String = ""
    var description: String = ""
    var images: [URL] = []
    var address: String = ""
    var iptu: String = ""
    var squareValue: String = ""
    var condoFee: String = ""
    var bedrooms: Int = 0
    var bathroooms: Int = 0
    var parkingSpots: Int = 0
    var area: String = ""
    var portals: Set<Portal> = []
    
    var haveImage: Bool {
        !images.isEmpty
    }
}
