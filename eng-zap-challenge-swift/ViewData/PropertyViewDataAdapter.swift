//
//  PropertyViewDataAdapter.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 14/02/21.
//

import Foundation

protocol PropertyViewDataAdapterProtocol {
    func adapt(_ model: Property) -> PropertyViewData
}


struct PropertyViewDataAdapter: PropertyViewDataAdapterProtocol {
    func adapt(_ model: Property) -> PropertyViewData {
        PropertyViewData(
            businessType: model.pricingInfo.businessType.title,
            price: getPrice(property: model),
            description: getDescription(property: model),
            images: model.images,
            address: getAddress(property: model),
            iptu: getIPTU(property: model),
            squareValue: getSquareValue(property: model),
            condoFee: getCondoFee(property: model),
            bedrooms: model.bedrooms,
            bathroooms: model.bathrooms,
            parkingSpots: model.parkingSpaces,
            area: getArea(property: model),
            portals: model.portals)
    }
}

fileprivate extension PropertyViewDataAdapter {
    
    func getPrice(property: Property) -> String {
        switch property.pricingInfo.businessType{
        case .rental:
            return "R$ \(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), property.pricingInfo.price))/mês"
        case .sale:
            return "R$ \(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), property.pricingInfo.price))"
        default:
            return "Indisponível"
        }
    }
    
    func getDescription(property: Property) -> String {
        "Quarto(s): \(property.bedrooms) | Banheiro(s): \(property.bathrooms) | Vaga(s): \(property.parkingSpaces) | Área: \(String(format: "%.0f", property.area))m²"
    }
    
    func getAddress(property: Property) -> String {
        guard property.address.neighborhood != "" && property.address.city != "" else {
            return ""
        }
        return "\(property.address.neighborhood), \(property.address.city)"
    }

    func getIPTU(property: Property) -> String {
        return "R$ \(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), property.pricingInfo.iptu))"
    }

    func getSquareValue(property: Property) -> String {
        return "R$ \(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), property.squareMeterValue)) m²"
    }
    
    func getCondoFee(property: Property) -> String {
        guard let condoFee = property.pricingInfo.condoFee else { return "Indisponível" }
        return "R$ \(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), condoFee))"
    }

    func getArea(property: Property) -> String {
        return "\(String(format: "%.0f", locale: Locale(identifier: "pt_BR"), property.area)) m"
    }
}
