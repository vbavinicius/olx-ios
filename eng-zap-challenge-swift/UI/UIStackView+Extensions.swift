//
//  UIStackView+Extensions.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 14/02/21.
//

import UIKit

extension UIStackView {
        
    static func infoStackView(title: String, value: String) -> UIStackView {
        let infoStackView: UIStackView = {
            let stackView = UIStackView()
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.axis = .vertical
            stackView.spacing = 4
            return stackView
        }()
        
        let infoTitleLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = title.uppercased()
            label.textAlignment = .center
            label.font = .systemFont(ofSize: 10, weight: .bold)
            label.textColor = .gray
            label.numberOfLines = 1
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            return label
        }()
        
        let infoValueLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = value.uppercased()
            label.textAlignment = .center
            label.font = .systemFont(ofSize: 13, weight: .light)
            label.textColor = .gray
            label.numberOfLines = 1
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            return label
        }()
        
        infoStackView.addArrangedSubview(infoTitleLabel)
        infoStackView.addArrangedSubview(infoValueLabel)
        return infoStackView
    }
}
