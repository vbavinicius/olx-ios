//
//  ViewState.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import Foundation

enum ViewState {
    case loading, loaded, error
}
