//
//  LoadingView.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import UIKit

class LoadingView: UIView {
    
    private let loadingMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14, weight: .regular)
        label.numberOfLines = 0
        label.textColor = .systemGray
        return label
    }()
    
    private let activityIndicatorView: UIActivityIndicatorView = {
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        activityIndicatorView.hidesWhenStopped = true
        return activityIndicatorView
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()

    init(loadingMessage: String = "") {
        super.init(frame: .zero)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        setupViews()
        setupHierarchy()
        setupConstraints()
        
        self.loadingMessageLabel.text = loadingMessage
    }
    
    public override init(frame: CGRect) {
        fatalError()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}


extension LoadingView {
    
    private func setupHierarchy() {
        stackView.addArrangedSubview(activityIndicatorView)
        stackView.addArrangedSubview(loadingMessageLabel)
        addSubview(stackView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16)
        ])
    }
    
    private func setupViews() {
        backgroundColor = .white
        activityIndicatorView.startAnimating()
    }

    func show(on viewcontroller: UIViewController, above aboveView: UIView, animated: Bool) {
        viewcontroller.view.insertSubview(self, aboveSubview: aboveView)
        NSLayoutConstraint.activate([
            self.topAnchor.constraint(equalTo: viewcontroller.view.safeAreaLayoutGuide.topAnchor),
            self.bottomAnchor.constraint(equalTo: viewcontroller.view.safeAreaLayoutGuide.bottomAnchor),
            self.leadingAnchor.constraint(equalTo: viewcontroller.view.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: viewcontroller.view.trailingAnchor)
        ])
        self.layoutIfNeeded()
    }
}
