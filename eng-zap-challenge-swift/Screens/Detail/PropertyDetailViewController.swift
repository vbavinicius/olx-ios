//
//  PropertyDetailViewController.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import UIKit

class PropertyDetailViewController: UIViewController {
    
    let viewData: PropertyViewData
    
    private lazy var imagesScrollView: PropertyDetailImageView = {
        let imagesScrollView = PropertyDetailImageView(viewData: viewData)
        imagesScrollView.translatesAutoresizingMaskIntoConstraints = false
        return imagesScrollView
    }()
    
    private lazy var priceView: PropertyDetailPriceView = {
        let priceInfoView = PropertyDetailPriceView(viewData: viewData)
        priceInfoView.translatesAutoresizingMaskIntoConstraints = false
        return priceInfoView
    }()
    
    private lazy var infoView: PropertyDetailInfoView = {
        let priceInfoView = PropertyDetailInfoView(viewData: viewData)
        priceInfoView.translatesAutoresizingMaskIntoConstraints = false
        return priceInfoView
    }()
    
    private lazy var addressLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = viewData.address
        label.font = .systemFont(ofSize: 14, weight: .bold)
        label.textColor = .gray
        label.numberOfLines = 2
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    init(viewData: PropertyViewData) {
        self.viewData = viewData
        super.init(nibName: nil, bundle: nil)
        setupHierarchy()
        setupConstraints()
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
}


extension PropertyDetailViewController {
    
    private func setupHierarchy() {
        view.addSubview(imagesScrollView)
        view.addSubview(priceView)
        view.addSubview(infoView)
        view.addSubview(addressLabel)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            imagesScrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            imagesScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            imagesScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            addressLabel.topAnchor.constraint(equalTo: imagesScrollView.bottomAnchor, constant: 16),
            addressLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            addressLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            
            priceView.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: 24),
            priceView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            priceView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            
            infoView.topAnchor.constraint(equalTo: priceView.bottomAnchor, constant: 16),
            infoView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            infoView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    private func setupViews() {
        view.backgroundColor = .white
        title = viewData.businessType.description
    }
}
