//
//  PropertyDetailInfoView.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import UIKit

class PropertyDetailInfoView: UIView {
    
    private let viewData: PropertyViewData
    private var bedroomsStackView: UIStackView?
    private var bathroomsStackView: UIStackView?
    private var parkingStackView: UIStackView?
    private var areaStackView: UIStackView?
    
    private let priceInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 4
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        return stackView
    }()

    init(viewData: PropertyViewData) {
        self.viewData = viewData
        super.init(frame : .zero)
        
        self.bedroomsStackView = UIStackView.infoStackView(title: "QUARTOS", value: "\(viewData.bedrooms)")
        self.bathroomsStackView = UIStackView.infoStackView(title: "BANHEIROS", value: "\(viewData.bathroooms)")
        self.parkingStackView = UIStackView.infoStackView(title: "VAGAS", value: "\(viewData.parkingSpots)")
        self.areaStackView = UIStackView.infoStackView(title: "ÁREA", value: "\(viewData.area)")
        
        setupHierarchy()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}

extension PropertyDetailInfoView {
    
    private func setupHierarchy() {
        addSubview(priceInfoStackView)
        if let bedroomsStackView = bedroomsStackView {
            priceInfoStackView.addArrangedSubview(bedroomsStackView)
        }
        if let bathroomsStackView = bathroomsStackView {
            priceInfoStackView.addArrangedSubview(bathroomsStackView)
        }
        if let parkingStackView = parkingStackView {
            priceInfoStackView.addArrangedSubview(parkingStackView)
        }
        if let areaStackView = areaStackView {
            priceInfoStackView.addArrangedSubview(areaStackView)
        }
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            priceInfoStackView.topAnchor.constraint(equalTo: topAnchor),
            priceInfoStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            priceInfoStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            priceInfoStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
        ])
    }
}
