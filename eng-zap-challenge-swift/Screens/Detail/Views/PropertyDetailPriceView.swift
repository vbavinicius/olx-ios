//
//  PropertyDetailPriceView.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import UIKit

class PropertyDetailPriceView: UIView {
    
    private let viewData: PropertyViewData
    private var priceStackView: UIStackView?
    private var condoStackView: UIStackView?
    private var iptuStackView: UIStackView?
    private var squareValueStackView: UIStackView?
    
    private let priceInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 4
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        return stackView
    }()

    init(viewData: PropertyViewData) {
        self.viewData = viewData
        super.init(frame : .zero)
        
        self.priceStackView = UIStackView.infoStackView(title: "Preço", value: viewData.price)
        self.condoStackView = UIStackView.infoStackView(title: "Condomínio", value: viewData.condoFee)
        self.iptuStackView = UIStackView.infoStackView(title: "IPTU", value: viewData.iptu)
        self.squareValueStackView = UIStackView.infoStackView(title: "Valor m", value: viewData.squareValue)
        
        setupHierarchy()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}

extension PropertyDetailPriceView {
    
    private func setupHierarchy() {
        addSubview(priceInfoStackView)
        if let priceStackView = priceStackView {
            priceInfoStackView.addArrangedSubview(priceStackView)
        }
        if let condoStackView = condoStackView {
            priceInfoStackView.addArrangedSubview(condoStackView)
        }
        if let iptuStackView = iptuStackView {
            priceInfoStackView.addArrangedSubview(iptuStackView)
        }
        if let squareValueStackView = squareValueStackView {
            priceInfoStackView.addArrangedSubview(squareValueStackView)
        }
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            priceInfoStackView.topAnchor.constraint(equalTo: topAnchor),
            priceInfoStackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            priceInfoStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            priceInfoStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
        ])
    }
}
