//
//  PropertyDetailImageView.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 13/02/21.
//

import UIKit

class PropertyDetailImageView: UIView {
    
    private let viewData: PropertyViewData
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.numberOfPages = viewData.images.count
        pageControl.currentPage = 0
        pageControl.pageIndicatorTintColor = .lightGray
        pageControl.currentPageIndicatorTintColor = .darkGray
        return pageControl
    }()
    
    init(viewData: PropertyViewData) {
        self.viewData = viewData
        super.init(frame : .zero)
        setupHierarchy()
        setupConstraints()

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupScrollView()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}


extension PropertyDetailImageView {
    
    private func setupHierarchy() {
        addSubview(scrollView)
        addSubview(pageControl)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            NSLayoutConstraint(item: scrollView,
                               attribute: .height,
                               relatedBy: .equal,
                               toItem: scrollView,
                               attribute: .width,
                               multiplier: 0.75,
                               constant: 0),
            
            pageControl.centerXAnchor.constraint(equalTo: centerXAnchor),
            pageControl.heightAnchor.constraint(equalToConstant: 30),
            pageControl.topAnchor.constraint(equalTo: scrollView.bottomAnchor),
            pageControl.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
    private func setupScrollView() {
        let frame = scrollView.frame
        scrollView.contentSize = CGSize(width: frame.width * CGFloat(viewData.images.count),
                                        height: frame.height)

        for (index, _) in viewData.images.enumerated() {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFill
            imageView.layer.masksToBounds = true
            imageView.backgroundColor = .lightGray
            imageView.tintColor = .gray
            imageView.setImage(viewData.images[index], withPlaceholder: "house-placeholder")
            
            scrollView.addSubview(imageView)
            imageView.frame = CGRect(x: frame.width * CGFloat(index),
                                     y: 0,
                                     width: frame.width,
                                     height: frame.height)
        }
        
        scrollView.delegate = self
    }
}


extension PropertyDetailImageView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / scrollView.frame.width)
        pageControl.currentPage = Int(pageIndex)
    }
}
