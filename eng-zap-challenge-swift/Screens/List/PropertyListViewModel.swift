//
//  PropertyListViewModel.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation
import Combine

class PropertyListViewModel {
    
    private let propertiesService: PropertiesServiceProtocol
    private let propertyViewDataAdapter: PropertyViewDataAdapterProtocol
    private var state: ViewState = .loaded
    let availablePortals: [Portal] = Portal.allCases
    
    private var viewData: [PropertyViewData] = []
    var filteredViewData: [PropertyViewData] {
        viewData.filter { $0.portals.contains(availablePortals[selectedPortalIndex]) }
    }
    
    var selectedPortalIndex: Int = 0 {
        didSet {
            viewStateSubject.send(.loaded)
        }
    }
    
    let output: Output
    private let viewStateSubject = PassthroughSubject<ViewState, Never>()
    
    init(propertiesService: PropertiesServiceProtocol = PropertiesService(),
         propertyViewDataAdapter: PropertyViewDataAdapterProtocol = PropertyViewDataAdapter()) {
        self.propertiesService = propertiesService
        self.propertyViewDataAdapter = propertyViewDataAdapter
        self.output = Output(state: viewStateSubject.eraseToAnyPublisher())
    }
}


extension PropertyListViewModel {
    struct Output {
        let state: AnyPublisher<ViewState, Never>
    }
}


extension PropertyListViewModel {
    
    func fetchProperties() {
        viewStateSubject.send(.loading)
        propertiesService.getAvailableProperties { [unowned self] result in
            switch result {
            case .success(let properties):
                self.viewData = properties.map { self.propertyViewDataAdapter.adapt($0) }
                self.viewStateSubject.send(.loaded)
            case .failure:
                self.viewStateSubject.send(.error)
            }
        }
    }
    
    func changeIndex(to index: Int) {
        selectedPortalIndex = index
    }
}


