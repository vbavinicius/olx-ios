//
//  ViewController.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import UIKit
import Combine

class PropertyListViewController: UIViewController {

    private let viewModel: PropertyListViewModel
    private var bag = Set<AnyCancellable>()
    
    private let loadingView = LoadingView(loadingMessage: "Carregando imóveis")
    private let errorView = ErrorView(errorMessage: "Erro ao carregar imóveis")
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    private lazy var segmentedControl: UISegmentedControl = {
        let items = viewModel.availablePortals
        let segmentedController = UISegmentedControl(items: items.map { $0.title })
        segmentedController.selectedSegmentIndex = viewModel.selectedPortalIndex
        segmentedController.isEnabled = false
        segmentedController.addTarget(self, action: #selector(segmentDidChanged(sender:)), for: .valueChanged)
        return segmentedController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHierarchy()
        setupConstraints()
        setupViews()
        bindViewModel()
        
        viewModel.fetchProperties()
    }
    
    init(viewModel: PropertyListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError()
    }
}


extension PropertyListViewController {
    
    private func bindViewModel() {
        viewModel.output.state.sink { [unowned self] state in
            resetView()
            switch state {
            case .loading:
                self.loadingView.show(on: self, above: self.tableView, animated: true)
            case .loaded:
                self.segmentedControl.isEnabled = true
                self.title = viewModel.availablePortals[viewModel.selectedPortalIndex].title
                self.tableView.reloadData()
            case .error:
                self.errorView.show(error: APIError.generic, on: self, above: self.tableView) {
                    self.viewModel.fetchProperties()
                }
            }
        }.store(in: &bag)
    }
    
    private func resetView() {
        self.segmentedControl.isEnabled = false
        self.loadingView.removeFromSuperview()
        self.errorView.removeFromSuperview()
    }
}


extension PropertyListViewController {
    
    private func setupHierarchy() {
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
        ])
    }
    
    private func setupViews() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(PropertyListTableViewCell.self, forCellReuseIdentifier: PropertyListTableViewCell.reuseIdentifier)
        
        navigationItem.titleView = segmentedControl
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}


extension PropertyListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filteredViewData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PropertyListTableViewCell.reuseIdentifier) as! PropertyListTableViewCell
        cell.configure(viewData: viewModel.filteredViewData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let property = viewModel.filteredViewData[indexPath.row]
        navigationController?.pushViewController(PropertyDetailViewController(viewData: property), animated: true)
    }
}


extension PropertyListViewController {
    @objc
    private func segmentDidChanged(sender: UISegmentedControl) {
        viewModel.changeIndex(to: segmentedControl.selectedSegmentIndex)
    }
}
