//
//  PropertyListTableViewCell.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import UIKit

class PropertyListTableViewCell: UITableViewCell {
    
    private let propertyImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .lightGray
        imageView.layer.cornerRadius = 8
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.tintColor = .gray
        return imageView
    }()
    
    private let propertyInfoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    
    private let propertyBusinessTypeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 12, weight: .bold)
        label.textColor = .gray
        return label
    }()
    
    private let propertyBusinessPriceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 16, weight: .bold)
        label.textColor = .darkGray
        return label
    }()
    
    private let propertyBusinessInfoLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 10, weight: .light)
        label.textColor = .gray
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupHierarchy()
        setupConstraints()
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}


extension PropertyListTableViewCell {
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.propertyBusinessTypeLabel.text = ""
        self.propertyBusinessPriceLabel.text = ""
        self.propertyBusinessInfoLabel.text = ""
        self.propertyImageView.image = nil
    }
    
    func configure(viewData: PropertyViewData) {
        self.propertyBusinessTypeLabel.text = viewData.businessType.uppercased()
        self.propertyBusinessPriceLabel.text = viewData.price
        self.propertyBusinessInfoLabel.text = viewData.description
        self.propertyImageView.image = UIImage(named: "house-placeholder")
        if viewData.haveImage {
            self.propertyImageView.setImage(viewData.images[0], withPlaceholder: "house-placeholder")
        }
    }
    
}


extension PropertyListTableViewCell {
    
    private func setupHierarchy() {
        addSubview(propertyImageView)
        addSubview(propertyInfoStackView)
        propertyInfoStackView.addArrangedSubview(propertyBusinessTypeLabel)
        propertyInfoStackView.addArrangedSubview(propertyBusinessPriceLabel)
        propertyInfoStackView.addArrangedSubview(propertyBusinessInfoLabel)
    }
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            propertyImageView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            propertyImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            propertyImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            propertyImageView.heightAnchor.constraint(equalToConstant: 90),
            propertyImageView.widthAnchor.constraint(equalToConstant: 120),
            
            propertyInfoStackView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            propertyInfoStackView.leadingAnchor.constraint(equalTo: propertyImageView.trailingAnchor, constant: 8),
            propertyInfoStackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
    }
    
    private func setupViews() {
        selectionStyle = .none
    }
    
}
