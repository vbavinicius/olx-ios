//
//  Portal.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

enum Portal: CaseIterable {
    case zap, vivareal
    
    var title: String {
        switch self {
        case .zap:
            return "Zap Imóveis"
        case .vivareal:
            return "Viva Real"
        }
    }
}
