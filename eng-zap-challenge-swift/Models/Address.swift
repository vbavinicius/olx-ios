//
//  Address.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

struct Address {
    let city: String
    let neighborhood: String
    let longitude: Double
    let latitude: Double
}

extension Address: Equatable {}
