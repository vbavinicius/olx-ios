//
//  BusinessType.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

enum BusinessType {
    case rental, sale, unknowed
    
    var title: String {
        switch self {
        case .rental:
            return "Aluguel"
        case .sale:
            return "Venda"
        case .unknowed:
            return "Indefinido"
        }
    }
}
