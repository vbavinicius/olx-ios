//
//  ZapBoundingBox.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

struct ZapBoundingBox {
    
    let minLongitude: Double = -46.693419
    let maxLongitude: Double = -46.641146
    let minLatitude: Double = -23.568704
    let maxLatitude: Double = -23.546686
    
    func propertyIsInZapBoundingBox(property: Property) -> Bool {
        isInZapBoundingBox(latitude: property.address.latitude, longitude: property.address.longitude)
    }
    
    internal func isInZapBoundingBox(latitude: Double, longitude: Double) -> Bool {
        (minLatitude...maxLatitude).contains(latitude) && (minLongitude...maxLongitude).contains(longitude)
    }
}
