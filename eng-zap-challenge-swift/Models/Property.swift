//
//  Property.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

struct Property {
    let area: Double
    let id: String
    let parkingSpaces: Int
    let images: [URL]
    let bedrooms: Int
    let bathrooms: Int
    let address: Address
    var pricingInfo: PricingInfo
    
    var portals: Set<Portal>
}

extension Property {
    
    var condoFeeProportion: Double? {
        guard let condoFee = pricingInfo.condoFee else { return nil }
        return (condoFee / pricingInfo.price) * 100
    }
    
    var squareMeterValue: Double {
        guard area != 0 else { return pricingInfo.price }
        return pricingInfo.price / area
    }
    
    var isInZapBoundbox: Bool {
        return ZapBoundingBox().propertyIsInZapBoundingBox(property: self)
    }
    
}


extension Property: Equatable {}
