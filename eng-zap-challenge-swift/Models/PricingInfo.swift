//
//  PricingInfo.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

struct PricingInfo {
    var price: Double
    let iptu: Double
    let businessType: BusinessType
    let condoFee: Double?
}

extension PricingInfo: Equatable {}
