//
//  RequestMapper.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

let _HOST_ = "grupozap-code-challenge.s3-website-us-east-1.amazonaws.com"

protocol RequestMapperProtocol {
    func createRequest(for route: APIRoute) -> URLRequest?
}

struct RequestMapper: RequestMapperProtocol {

    let baseURLString: String = _HOST_
    
    func createRequest(for route: APIRoute) -> URLRequest? {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = baseURLString
        urlComponents.path = route.path

        guard let url = urlComponents.url else { return nil }
        return URLRequest(url: url)
    }
}
