//
//  PropertiesRoute.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

enum PropertiesRoute: APIRoute {
    
    case getProperties
    
    var path: String {
        "/sources/source-1.json"
    }
    
    var method: HTTPMethod {
        .get
    }
}
