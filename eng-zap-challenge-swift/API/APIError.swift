//
//  APIError.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

enum APIError: Error {
    case notConnectedToInternet
    case invalidData
    case generic
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .notConnectedToInternet:
            return "Parece que você está sem conexão"
        case .invalidData, .generic:
            return "Algo de errado aconteceu"
        }
    }
}
