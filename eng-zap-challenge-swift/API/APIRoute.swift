//
//  APIRoute.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

public protocol APIRoute {
    var path: String { get }
    var method: HTTPMethod { get }
}

public enum HTTPMethod: String {
    case get = "GET"
}
