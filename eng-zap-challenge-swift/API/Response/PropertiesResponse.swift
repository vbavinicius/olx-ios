//
//  PropertiesResponse.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

struct PropertyResponse: Codable {
    let area: Double
    let id: String
    let parkingSpaces: Int?
    let images: [URL]
    let bedrooms: Int?
    let bathrooms: Int?
    let address: AddressResponse
    let pricingInfos: PricingInfoResponse

    enum CodingKeys: String, CodingKey {
        case id, parkingSpaces, images, bedrooms, bathrooms, address, pricingInfos
        case area = "usableAreas"
    }
}

struct AddressResponse: Codable {
    let city: String
    let neighborhood: String
    let geoLocation: GeoLocationResponse
}

struct GeoLocationResponse: Codable {
    let location: LocationResponse
}

struct LocationResponse: Codable {
    let longitude: Double
    let latitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lon"
    }
}

struct PricingInfoResponse: Codable {
    let iptu: String?
    let price: String?
    let businessType: String
    let condoFee: String?
    
    enum CodingKeys: String, CodingKey {
        case price, businessType
        case iptu = "yearlyIptu"
        case condoFee = "monthlyCondoFee"
    }
}


extension PropertyResponse: Equatable {
    static func == (lhs: PropertyResponse, rhs: PropertyResponse) -> Bool {
        lhs.id == rhs.id
    }
}
