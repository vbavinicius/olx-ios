//
//  PropertiesAPI.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import Foundation

protocol PropertiesAPIProtocol {
    func getProperties(completion: @escaping (Result<[PropertyResponse], APIError>) -> Void)
}

struct PropertiesAPI: PropertiesAPIProtocol {
    
    let client: APIClientProtocol
    
    init(client: APIClientProtocol = APIClient()) {
        self.client = client
    }
    
    func getProperties(completion: @escaping (Result<[PropertyResponse], APIError>) -> Void) {
        let route = PropertiesRoute.getProperties
        client.request(route, returnQueue: .main) { result in
            switch result {
            case .success(let data):
                do {
                    let propertiesAPIResponse = try JSONDecoder().decode([PropertyResponse].self, from: data)
                    completion(.success(propertiesAPIResponse))
                } catch {
                    completion(.failure(APIError.invalidData))
                }
            case .failure(let apiError):
                 completion(.failure(apiError))
            }
        }
    }
    
}
