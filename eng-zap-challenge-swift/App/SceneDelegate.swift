//
//  SceneDelegate.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 10/02/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.windowScene = windowScene
        let viewController = PropertyListViewController(viewModel: PropertyListViewModel())
        window?.rootViewController = UINavigationController(rootViewController: viewController)
        window?.rootViewController?.view.backgroundColor = .white
        window?.makeKeyAndVisible()
    }
}

