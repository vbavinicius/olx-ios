//
//  PropertiesService.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

protocol PropertiesServiceProtocol {
    func getAvailableProperties(completion: @escaping (Result<[Property], APIError>) -> Void)
}

struct PropertiesService: PropertiesServiceProtocol {
    
    let propertiesAPI: PropertiesAPIProtocol
    
    init(propertiesAPI: PropertiesAPIProtocol = PropertiesAPI()) {
        self.propertiesAPI = propertiesAPI
    }
    
    func getAvailableProperties(completion: @escaping (Result<[Property], APIError>) -> Void) {
        propertiesAPI.getProperties { result in
            switch result {
            case .success(let propertiesResponse):
                completion(.success(propertiesResponse.map { PropertyAdapter().adapt($0) }))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}
