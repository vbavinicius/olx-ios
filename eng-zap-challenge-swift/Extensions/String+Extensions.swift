//
//  String+Extensios.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation

extension String {
    func doubleValue() -> Double? {
        Double(self) ?? 0
    }
    
    func optionalDoubleValue() -> Double? {
        Double(self)
    }
}
