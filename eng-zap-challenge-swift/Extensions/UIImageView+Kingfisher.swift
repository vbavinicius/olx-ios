//
//  UIImageView+Kingfisher.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import UIKit
import Kingfisher

extension UIImageView {
    func setImage(_ url: URL, withPlaceholder placeholder: String) {
        self.kf.setImage(
        with: url,
        placeholder: UIImage(named: placeholder),
        options: [
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(0.3)),
            .cacheOriginalImage
        ])
    }
}
