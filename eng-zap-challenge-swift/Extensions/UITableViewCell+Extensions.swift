//
//  UITableViewCell+Extensions.swift
//  eng-zap-challenge-swift
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
      return String(describing: self)
    }
}
