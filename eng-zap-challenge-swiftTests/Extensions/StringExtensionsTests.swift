//
//  StringExtensionsTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class StringExtensionsTests: XCTestCase {
    func test_StringConvertsToDouble_SucceedsWhenReceiveValidValue() {
        XCTAssertEqual("123".doubleValue(), 123)
        XCTAssertEqual("123.1231".doubleValue(), 123.1231)
        XCTAssertEqual("-123".doubleValue(), -123)
        XCTAssertEqual("-123.1231".doubleValue(), -123.1231)
        XCTAssertEqual("0".doubleValue(), 0)
        XCTAssertEqual("123asda".doubleValue(), 0)
    }
}
