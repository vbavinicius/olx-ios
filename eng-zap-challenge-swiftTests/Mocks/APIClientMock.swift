//
//  APIClientMock.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation
@testable import eng_zap_challenge_swift

class APIClientMock: APIClientProtocol {
    
    public var invokedAPIClientRequest = false
    public var invokedAPIClientCount = 0
    public var mockedCompletion: (Result<Data, APIError>)?
    
    public var onRequest: ((APIRoute, DispatchQueue?) -> Void)?
    
    func request(_ route: APIRoute, returnQueue: DispatchQueue?, completion: @escaping (Result<Data, APIError>) -> Void) {
        if let onRequest = onRequest {
            onRequest(route, returnQueue)
        }
        invokedAPIClientRequest = true
        invokedAPIClientCount += 1
        if let mockedCompletion = mockedCompletion {
            completion(mockedCompletion)
        }
    }
}
