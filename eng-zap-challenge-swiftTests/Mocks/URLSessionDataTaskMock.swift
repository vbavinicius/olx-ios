//
//  URLSessionDataTaskMock.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation
@testable import eng_zap_challenge_swift

class URLSessionMock: URLSessionProtocol {
    
    var data: Data? = nil
    var error: Error? = nil
    var urlResponse: URLResponse? = nil
    
    @discardableResult
    func dataTask(with urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        completionHandler(data, urlResponse, error)
        return URLSessionDataTaskMock()
    }
}


class URLSessionDataTaskMock: URLSessionDataTask {
    override init() {}
    override func resume() {}
}
