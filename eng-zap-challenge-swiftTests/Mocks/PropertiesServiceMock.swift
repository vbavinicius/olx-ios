//
//  PropertiesServiceMock.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import Foundation
@testable import eng_zap_challenge_swift

class PropertiesServiceMock: PropertiesServiceProtocol {

    public var invokedPropertiesServiceRequest = false
    public var invokedAPropertiesServiceCount = 0
    public var mockedCompletion: (Result<[Property], APIError>)?
    
    func getAvailableProperties(completion: @escaping (Result<[Property], APIError>) -> Void) {
        invokedPropertiesServiceRequest = true
        invokedAPropertiesServiceCount += 1
        if let mockedCompletion = mockedCompletion {
            completion(mockedCompletion)
        }
    }
}
