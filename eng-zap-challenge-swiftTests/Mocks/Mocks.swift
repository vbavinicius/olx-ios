//
//  Helpers.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation
@testable import eng_zap_challenge_swift

enum Mock {}

extension Mock {
    static let mockURL = URL(string: "https://www.olx.com.br")!
    static let mock200Response = HTTPURLResponse(url: Mock.mockURL, statusCode: 200, httpVersion: nil, headerFields: [:])
    static let mock500Response = HTTPURLResponse(url: Mock.mockURL, statusCode: 500, httpVersion: nil, headerFields: [:])
    static let mockData = "mockdat".data(using: .utf8)
    
    static let zapProperty = Property(area: 100, id: "aa", parkingSpaces: 1, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 10, businessType: .sale, condoFee: 100), portals: [.zap])
    
    static let vivaRealProperty = Property(area: 100, id: "aa", parkingSpaces: 1, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 10, businessType: .sale, condoFee: 100), portals: [.vivareal])
}
