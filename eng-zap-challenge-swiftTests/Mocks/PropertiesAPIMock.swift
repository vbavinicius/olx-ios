//
//  PropertiesAPIMock.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import Foundation
@testable import eng_zap_challenge_swift

class PropertiesAPIMock: PropertiesAPIProtocol {
    
    public var invokedPropertiesAPIRequest = false
    public var invokedAPropertiesAPICount = 0
    public var mockedCompletion: (Result<[PropertyResponse], APIError>)?
    
    func getProperties(completion: @escaping (Result<[PropertyResponse], APIError>) -> Void) {
        invokedPropertiesAPIRequest = true
        invokedAPropertiesAPICount += 1
        if let mockedCompletion = mockedCompletion {
            completion(mockedCompletion)
        }
    }
}
