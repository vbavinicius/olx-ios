//
//  PropertiesServiceTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class PropertiesServiceTests: XCTestCase {
    
    var sut: PropertiesService!
    var propertiesAPIMock: PropertiesAPIMock!
    
    override func setUp() {
        propertiesAPIMock = PropertiesAPIMock()
        sut = PropertiesService(propertiesAPI: propertiesAPIMock)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    
    func test_PropertiesServiceFails_WhenServiceAPIFails() {
        let expectedError: APIError = .generic
        
        propertiesAPIMock.mockedCompletion = .failure(expectedError)
        
        sut.getAvailableProperties { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(expectedError, error)
            }
        }
    
        XCTAssertEqual(propertiesAPIMock.invokedAPropertiesAPICount, 1)
        XCTAssertEqual(propertiesAPIMock.invokedPropertiesAPIRequest, true)
    }
    
    func test_PropertiesServiceSucceeds_WhenServiceAPISucceeds() {
        let value: [PropertyResponse] = Mock.getPropertiesResponse()
        let expectedValue = value.map { PropertyAdapter().adapt($0) }
        
        propertiesAPIMock.mockedCompletion = .success(value)
        
        sut.getAvailableProperties { result in
            switch result {
            case .success(let value):
                XCTAssertEqual(value, expectedValue)
            case .failure:
                XCTFail()
            }
        }
    
        XCTAssertEqual(propertiesAPIMock.invokedAPropertiesAPICount, 1)
        XCTAssertEqual(propertiesAPIMock.invokedPropertiesAPIRequest, true)
    }
    
}
