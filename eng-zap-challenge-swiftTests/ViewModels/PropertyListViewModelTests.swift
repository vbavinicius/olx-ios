//
//  PropertyListViewModelTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 12/02/21.
//

import XCTest
import Combine
@testable import eng_zap_challenge_swift

class PropertyListViewModelTests: XCTestCase {

    var sut: PropertyListViewModel!
    var propertiesService: PropertiesServiceMock!
    var bag: Set<AnyCancellable> = []
    
    override func setUp() {
        propertiesService = PropertiesServiceMock()
        sut = PropertyListViewModel(propertiesService: propertiesService)
    }
    
    override func tearDown() {
        propertiesService = nil
        sut = nil
    }
    
    func test_ViewModelSendLoadingState_WhenStartsFetch() {
        let expectedState: ViewState = .loading
        var receivedState: ViewState!
        
        sut.output.state.sink { state in
            receivedState = state
        }.store(in: &bag)
        
        sut.fetchProperties()
        
        XCTAssertNotNil(receivedState)
        XCTAssertEqual(expectedState, receivedState)
    }
    
    func test_ViewModelSendLoadedState_WhenServiceSucceds() {
        let expectedState: ViewState = .loaded
        var receivedState: ViewState!
        
        propertiesService.mockedCompletion = .success([])
        
        sut.output.state
            .dropFirst()
            .sink { state in
            receivedState = state
        }.store(in: &bag)
        
        sut.fetchProperties()
        
        XCTAssertNotNil(receivedState)
        XCTAssertEqual(expectedState, receivedState)
    }
    
    func test_ViewModelSendErrorState_WhenServiceFails() {
        let expectedState: ViewState = .error
        var receivedState: ViewState!
        
        propertiesService.mockedCompletion = .failure(.generic)
        
        sut.output.state
            .dropFirst()
            .sink { state in
            receivedState = state
        }.store(in: &bag)
        
        sut.fetchProperties()
        
        XCTAssertNotNil(receivedState)
        XCTAssertEqual(expectedState, receivedState)
    }
    
    func test_ViewModelSendCorrectPortalData_WhenServiceFails() {

        propertiesService.mockedCompletion = .success([Mock.zapProperty, Mock.vivaRealProperty])
        
        sut.fetchProperties()
        
        sut.changeIndex(to: 0)
        
        XCTAssert(sut.filteredViewData.contains { $0.portals.contains(.zap)})
        XCTAssertFalse(sut.filteredViewData.contains { $0.portals.contains(.vivareal)})
        
        sut.changeIndex(to: 1)
        
        XCTAssert(sut.filteredViewData.contains { $0.portals.contains(.vivareal)})
        XCTAssertFalse(sut.filteredViewData.contains { $0.portals.contains(.zap)})
    }
}
