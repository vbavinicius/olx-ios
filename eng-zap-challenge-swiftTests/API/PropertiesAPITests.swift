//
//  PropertiesAPITests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class PropertiesAPITests: XCTestCase {

    var sut: PropertiesAPI!
    var apiClientMock: APIClientMock!
    
    override func setUp() {
        apiClientMock = APIClientMock()
        sut = PropertiesAPI(client: apiClientMock)
    }

    override func tearDown() {
        apiClientMock = nil
        sut = nil
    }
    
    
    func test_PropertiesAPIGetProperties_UsesCorrectRoute() {
        let expectedRoute = PropertiesRoute.getProperties
        var receivedRoute: APIRoute?
        
        apiClientMock.onRequest = { (route, _) in
            receivedRoute = route
        }
        
        sut.getProperties { _ in }
        
        XCTAssertEqual(expectedRoute, receivedRoute as? PropertiesRoute)
        XCTAssertNotNil(receivedRoute)
        XCTAssertEqual(apiClientMock.invokedAPIClientCount, 1)
        XCTAssertEqual(apiClientMock.invokedAPIClientRequest, true)
    }


    func test_PropertiesAPIFails_WhenAPIClientFails() {
        let expectedError: APIError = .generic
        
        apiClientMock.mockedCompletion = .failure(expectedError)
        
        sut.getProperties { result in
            switch result {
            case .success:
                XCTFail()
            case .failure(let error):
                XCTAssertEqual(expectedError, error)
            }
        }
        
        XCTAssertEqual(apiClientMock.invokedAPIClientCount, 1)
        XCTAssertEqual(apiClientMock.invokedAPIClientRequest, true)
    }
    
    
    func test_PropertiesAPISucceeds_WhenAPIClientSucceeds() {
        let expectedValue: [PropertyResponse] = Mock.getPropertiesResponse()
        
        apiClientMock.mockedCompletion = .success(Mock.getPropertiesData())
        
        sut.getProperties { result in
            switch result {
            case .success(let properties):
                XCTAssertEqual(properties, expectedValue)
            case .failure:
                XCTFail()
            }
        }
        
        XCTAssertEqual(apiClientMock.invokedAPIClientCount, 1)
        XCTAssertEqual(apiClientMock.invokedAPIClientRequest, true)
    }
}
