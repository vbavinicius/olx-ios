//
//  ZapBoundingBoxTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class ZapBoundingBoxTests: XCTestCase {

    var sut: ZapBoundingBox!
    
    override func setUp() {
        sut = ZapBoundingBox()
    }

    override func tearDown() {
        sut = nil
    }
    
    func test_PositionInsideZapBoundingBox_ReturnsFalseIfIsNotInZapBoundingBox() {
        let longitude: Double = 1
        let latitude: Double = 1
        XCTAssertFalse(sut.isInZapBoundingBox(latitude: latitude, longitude: longitude))
    }
    
    func test_PositionInsideZapBoundingBox_ReturnsTrueIfIsInZapBoundingBox() {
        let longitude: Double = -46.65
        let latitude: Double = -23.55
        XCTAssert(sut.isInZapBoundingBox(latitude: latitude, longitude: longitude))
    }
    
    func test_PositionInsideZapBoundingBox_ReturnsTrueIfIsInZapBoundingBoxExactValues() {
        let longitude: Double = -46.693419
        let latitude: Double = -23.568704
        XCTAssert(sut.isInZapBoundingBox(latitude: latitude, longitude: longitude))
    }
}
