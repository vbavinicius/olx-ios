//
//  PropertyAdapterTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class PropertyAdapterTests: XCTestCase {

    var sut: PropertyAdapter!

    override func setUp() {
        sut = PropertyAdapter()
    }

    override func tearDown() {
        sut = nil
    }
    
    func test_PropertyWithNoLatitudeAndLongitude_AreNotListed() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 0, latitude: 0))), pricingInfos: PricingInfoResponse(iptu: "10", price: "100", businessType: "SALE", condoFee: "50"))
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.isEmpty)
    }
    
    func test_PropertyWithNoLatitude_AreListed() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 10, latitude: 0))), pricingInfos: PricingInfoResponse(iptu: "10", price: "10000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertFalse(model.portals.isEmpty)
    }
    
    func test_PropertyWithNoLongitude_AreListed() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 0, latitude: 10))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertFalse(model.portals.isEmpty)
    }
    
    func test_PropertyForSaleWithLess3500Square_AreNotListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
        XCTAssertFalse(model.portals.contains(.zap))
    }
    
    func test_PropertyForSaleWithMore3500Square_AreListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.zap))
    }
    
    func test_PropertyForSaleWith0Area_AreNotListesOnZap() {
        let propertyResponse = PropertyResponse(area: 0, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
        XCTAssertFalse(model.portals.contains(.zap))
    }
    
    func test_PropertyForRentWithLess30PercentCondo_AreListesOnVivaReal() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000", businessType: "rental", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
    }
    
    func test_PropertyForRentWithMore30PercentCondo_AreNotListesOnVivaReal() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "3000", businessType: "rental", condoFee: "1500"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertFalse(model.portals.contains(.vivareal))
    }
    
    func test_PropertyForRentWithInvalidCondoFee_AreNotListesOnVivaReal() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "10000", businessType: "rental", condoFee: "aaa"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.zap))
        XCTAssertFalse(model.portals.contains(.vivareal))
    }
    
    func test_PropertyForSaleIsInsideZapBoundBox_Get10PercentDiscount() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: -46.65, latitude: -23.55))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000000", businessType: "sale", condoFee: "aaa"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertEqual(model.pricingInfo.price, 900000)
    }
    
    func test_PropertyForSaleIsOutsideZapBoundBox_DontGet10PercentDiscount() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 90, latitude: 90))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000000", businessType: "sale", condoFee: "aaa"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertEqual(model.pricingInfo.price, 1000000)
    }
    
    func test_PropertyForRentInsideZapBoundBox_Get50PercentRaise() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: -46.65, latitude: -23.55))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000", businessType: "rental", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertEqual(model.pricingInfo.price, 1500)
    }
    
    func test_PropertyForRentOutsideZapBoundBox_DontGet50PercentRaise() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 90, latitude: 90))), pricingInfos: PricingInfoResponse(iptu: "10", price: "1000", businessType: "rental", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssertEqual(model.pricingInfo.price, 1000)
    }
    
    func test_PropertyForRentalWithLess3500Price_AreNotListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "2000", businessType: "rental", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
        XCTAssertFalse(model.portals.contains(.zap))
    }
    
    func test_PropertyForRentalWithLess3500Price_AreListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "5000", businessType: "rental", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.zap))
    }
    
    func test_PropertyForRentalWith3800Price_AreListesOnBoth() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "3800", businessType: "rental", condoFee: "1000"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.zap))
        XCTAssert(model.portals.contains(.vivareal))
    }
    
    func test_PropertyForSaleWithMore600000Price_AreListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "800000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.zap))
        XCTAssertFalse(model.portals.contains(.vivareal))
    }
    
    func test_PropertyForSaleWithLess600000Price_AreListesOnZap() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "500000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
        XCTAssertFalse(model.portals.contains(.zap))
    }
    
    func test_PropertyForSaleWith650000Price_AreListesOnBoth() {
        let propertyResponse = PropertyResponse(area: 10, id: "aa", parkingSpaces: 0, images: [], bedrooms: 2, bathrooms: 3, address: AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 100, latitude: 100))), pricingInfos: PricingInfoResponse(iptu: "10", price: "650000", businessType: "sale", condoFee: "50"))
        
        let model = sut.adapt(propertyResponse)
        XCTAssert(model.portals.contains(.vivareal))
        XCTAssert(model.portals.contains(.zap))
    }    
    
}
