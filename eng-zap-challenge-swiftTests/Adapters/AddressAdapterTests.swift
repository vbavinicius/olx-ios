//
//  AddressAdapterTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class AddressAdapterTests: XCTestCase {
    
    var sut: AddressAdapter!
    var response: AddressResponse!
    var model: Address!
    
    override func setUp() {
        sut = AddressAdapter()
        response = AddressResponse(city: "cidade", neighborhood: "bairro", geoLocation: GeoLocationResponse(location: LocationResponse(longitude: 10, latitude: 20)))
        model = Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 20)
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func test_adaptToModel() {
        let adapter = sut.adapt(response)
        XCTAssertEqual(adapter, model)
    }
    
}
