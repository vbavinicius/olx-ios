//
//  PropertyViewDataAdapterTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 14/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class PropertyViewDataAdapterTests: XCTestCase {

    var sut: PropertyViewDataAdapter!
    
    override func setUp() {
        sut = PropertyViewDataAdapter()
    }
    
    override func tearDown() {
        sut = nil
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectPrice_WhenRental() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.price, "R$ 100/mês")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectPrice_WhenSale() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.price, "R$ 100")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectDescription() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.description,
                       "Quarto(s): 2 | Banheiro(s): 2 | Vaga(s): 2 | Área: 10m²")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectImages() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.images, [])
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectAddress_WhenFullAddress() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.address, "bairro, cidade")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectAddress_IsMissingCity() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.address, "")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectAddress_WhenIsMissingNeighborhood() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.address, "")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectIPTU() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.iptu, "R$ 100")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectSquareValue_WhenIsValid() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.squareValue, "R$ 10 m²")
    }
    
    
    func test_PropertyViewDataAdapterAdaptsCorrectCondoFee() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 100, iptu: 100, businessType: .sale, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.condoFee, "R$ 100")
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectBedrooms() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.bedrooms, 2)
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectBathrooms() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.bathroooms, 2)
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectParkingSpots() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.parkingSpots, 2)
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectPortals() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.portals, [.vivareal, .zap])
    }
    
    func test_PropertyViewDataAdapterAdaptsCorrectArea() {
        let property = Property(area: 10, id: "aaa", parkingSpaces: 2, images: [], bedrooms: 2, bathrooms: 2, address: Address(city: "cidade", neighborhood: "bairro", longitude: 10, latitude: 10), pricingInfo: PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 100), portals: [.vivareal, .zap])
        
        let viewData = sut.adapt(property)
        
        XCTAssertEqual(viewData.area, "10 m")
    }
}
