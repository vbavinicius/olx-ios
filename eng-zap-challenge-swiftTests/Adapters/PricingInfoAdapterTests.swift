//
//  PricingInfoAdapterTests.swift
//  eng-zap-challenge-swiftTests
//
//  Created by Vinícius Barcelos on 11/02/21.
//

import XCTest
@testable import eng_zap_challenge_swift

class PricingInfoAdapterTests: XCTestCase {

    var sut: PricingInfoAdapter!

    override func setUp() {
        sut = PricingInfoAdapter()
    }

    override func tearDown() {
        sut = nil
    }

    func test_adaptToModelSale() {
        let response = PricingInfoResponse(iptu: "100", price: "1000", businessType: "SALE", condoFee: "200")
        let model = PricingInfo(price: 1000, iptu: 100, businessType: .sale, condoFee: 200)
        let adapter = sut.adapt(response)
        XCTAssertEqual(adapter, model)
    }

    func test_adaptToModelRental() {
        let response = PricingInfoResponse(iptu: "100", price: "1000", businessType: "RENTAL", condoFee: "200")
        let model = PricingInfo(price: 1000, iptu: 100, businessType: .rental, condoFee: 200)
        let adapter = sut.adapt(response)
        XCTAssertEqual(adapter, model)
    }
    
    func test_adaptToModelUnknow() {
        let response = PricingInfoResponse(iptu: "100", price: "1000", businessType: "AAA", condoFee: "200")
        let model = PricingInfo(price: 1000, iptu: 100, businessType: .unknowed, condoFee: 200)
        let adapter = sut.adapt(response)
        XCTAssertEqual(adapter, model)
    }

}
