# App Zap/Viva Real

O app é a entrega do desafio proposto pelo time de engenharia do Zap. Foi desenvolvido em Swift e XCode12.

- Para rodar localmente, basta rodar o arquivo .xcodeproj
- O projeto conta com apenas uma depência externa, o módulo Kingfisher para fazer o download de imagens. O módulo foi importado via SPM, e não cocoapods, portanto não é necessário rodar nenhum comando a mais para rodar o projeto. Basta abrir o XCode e aguardar o donwload do package.
- Todos os testes rodam no scheme eng-zap-challenge-swift via Product/Test do próprio XCode.


<img src="https://gitlab.com/vbavinicius/olx-ios/-/raw/master/example/olx.gif" height="350">
